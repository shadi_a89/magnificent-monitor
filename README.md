# magnificent-monitor

Java application for monitoring the http status code of any http server

This application will send an http response to the url that is set in property `app.service.url` and then log the
http status using log4j to console and log file and also send it to Statsd.

In addition to logging the http status, the success rate(responses with 200 status code) is also logged.

The rate at which new requests are sent is configurable using the property `app.monitor.freq.seconds`, default is 5

The success percentage report rate is configurable using the property `app.report.freq.seconds`, default is 30

### Log Format
For every http: {timestamp} - Response Status: {status}, for example `11:36:52.811 - Response Status: 200`

Report: {timestamp} - Success rate: {rate}, for example `11:37:17.794 - Success rate: 83.333336%`

### Run the application
First build the jar file using `maven clean install`, and then run `java -jar target/magnificent-monitor-1.0-SNAPSHOT-jar-with-dependencies.jar`

To run using docker first build the image `docker build -t service-monitor .`

Then use `docker run --network="host" service-monitor`

The image is also available on hub.docker.com, if you would like to pull and run the application use:

`docker run --network="host" -v ~/logs:/logs shadi/service-monitor`

`--network="host"` so that localhost is accessible from inside the docker container, `-v ~/logs:/logs` to be able 
to see the logs from the host machine

### Code EntryPoint
For Entrypoint look at class **MainClass**

The application works by scheduling two tasks at fixed rate, see `HeartbeatMonitorTask` for http requests that are sent 
to monitor the external service, and `OverallStatusTask` for the task that reports the success percentage.
 
[**Google Guice**](https://github.com/google/guice/wiki/GettingStarted) is used for
dependency injection, main bindings are in class **MonitorServiceModule**.