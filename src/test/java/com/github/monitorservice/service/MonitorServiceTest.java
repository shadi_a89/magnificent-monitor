package com.github.monitorservice.service;

import com.github.monitorservice.task.HeartbeatMonitorTask;
import org.junit.Test;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class MonitorServiceTest {

    public static final int FREQUENCY = 5;

    private final HeartbeatMonitorTask monitorTaskMock = mock(HeartbeatMonitorTask.class);
    private final ScheduledExecutorService executorServiceMock = mock(ScheduledExecutorService.class);

    private final MonitorService monitorService = new MonitorService(executorServiceMock,
                                                                     monitorTaskMock,
                                                                     FREQUENCY);

    @Test
    public void whenStartCalledThenScheduleTaskOnExecutorWithGivenFrequency() throws Exception {

        monitorService.start();

        verify(executorServiceMock).scheduleAtFixedRate(monitorTaskMock, 0, FREQUENCY, TimeUnit.SECONDS);
    }

    @Test
    public void whenShutdownCalledThenShutdownExecutor() throws Exception {
        monitorService.shutDown();

        verify(executorServiceMock).shutdown();
    }
}
