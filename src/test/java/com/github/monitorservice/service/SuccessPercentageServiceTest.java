package com.github.monitorservice.service;

import org.junit.Assert;
import org.junit.Test;

public class SuccessPercentageServiceTest {

    private final SuccessPercentageService service = new SuccessPercentageService();


    @Test
    public void whenCalledThenReturnSuccessPercentage() throws Exception {
        float httpSuccessPercentage = service.findHttpSuccessPercentage(2, 1, 1);

        Assert.assertEquals(50.0, httpSuccessPercentage, 0.01);
    }
}