package com.github.monitorservice.task;

import com.github.monitorservice.metrics.MetricsService;
import com.github.monitorservice.service.HttpStatusHistogram;
import com.github.monitorservice.service.http.HttpClient;
import com.github.monitorservice.service.http.MonitorHttpResponse;
import org.junit.Test;

import java.util.Optional;

import static org.mockito.Mockito.*;

public class HeartbeatMonitorTaskTest {

    public static final String URL = "url";

    private final MetricsService metricsServiceMock = mock(MetricsService.class);
    private final HttpClient httpClientMock = mock(HttpClient.class);
    private HttpStatusHistogram histogramMock = mock(HttpStatusHistogram.class);

    private final HeartbeatMonitorTask heartbeatMonitorTask = new HeartbeatMonitorTask(URL,
                                                                                       metricsServiceMock,
                                                                                       httpClientMock,
                                                                                       histogramMock);

    @Test
    public void whenHttpClientReturns200ResponseThenSendStatus() throws Exception {
        MonitorHttpResponse responseMock = mock(MonitorHttpResponse.class);
        int status = 200;
        when(responseMock.getStatus()).thenReturn(status);
        when(httpClientMock.get(URL)).thenReturn(Optional.of(responseMock));

        heartbeatMonitorTask.run();

        verify(metricsServiceMock).increment(String.valueOf(status));
        verify(histogramMock).incrementSuccess();
    }

    @Test
    public void whenHttpClientReturnsResponseThenSendStatus() throws Exception {
        MonitorHttpResponse responseMock = mock(MonitorHttpResponse.class);
        int status = 42;
        when(responseMock.getStatus()).thenReturn(status);
        when(httpClientMock.get(URL)).thenReturn(Optional.of(responseMock));

        heartbeatMonitorTask.run();

        verify(metricsServiceMock).increment(String.valueOf(status));
        verify(histogramMock).incrementNon200();
    }

    @Test
    public void whenHttpClientHasNoResponseThenLogError() throws Exception {
        when(httpClientMock.get(URL)).thenReturn(Optional.empty());

        heartbeatMonitorTask.run();

        verify(metricsServiceMock).increment("http-error");
    }
}