package com.github.monitorservice.metrics;

import com.timgroup.statsd.StatsDClient;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class MetricsServiceTest {

    private final StatsDClient statsDClientMock = mock(StatsDClient.class);

    private final MetricsService metricsService = new MetricsService(statsDClientMock);

    @Test
    public void givenCounterWhenIncrementIsCalledThenIncrementSameCounterOnStatsD() throws Exception {
        String counterName = "counter";

        metricsService.increment(counterName);

        verify(statsDClientMock).increment(counterName);
    }
}
