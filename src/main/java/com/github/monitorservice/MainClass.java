package com.github.monitorservice;

import com.github.monitorservice.service.MonitorService;
import com.github.monitorservice.service.OverallStatusReporter;
import com.google.inject.Guice;
import com.google.inject.Injector;


public class MainClass {

    public static void main(String[] args) {
        Injector injector = Guice.createInjector(new MonitorServiceModule());
        MonitorService monitorService = injector.getInstance(MonitorService.class);
        OverallStatusReporter statusReporter = injector.getInstance(OverallStatusReporter.class);

        statusReporter.startReporting();
        monitorService.start();
    }
}
