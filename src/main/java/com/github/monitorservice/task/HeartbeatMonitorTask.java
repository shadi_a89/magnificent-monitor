package com.github.monitorservice.task;

import com.github.monitorservice.metrics.MetricsService;
import com.github.monitorservice.service.HttpStatusHistogram;
import com.github.monitorservice.service.http.HttpClient;
import com.github.monitorservice.service.http.MonitorHttpResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.util.Optional;

@Singleton
public class HeartbeatMonitorTask implements Runnable {

    private static final Logger LOG = LogManager.getLogger(HeartbeatMonitorTask.class);


    private final String url;
    private final MetricsService metricsService;
    private final HttpClient httpClient;
    private final HttpStatusHistogram histogram;

    @Inject
    public HeartbeatMonitorTask(@Named("app.service.url") String url,
                                MetricsService metricsService,
                                HttpClient httpClient,
                                HttpStatusHistogram histogram) {
        this.url = url;
        this.metricsService = metricsService;
        this.httpClient = httpClient;
        this.histogram = histogram;
    }

    @Override
    public void run() {
        Optional<MonitorHttpResponse> monitorHttpResponse = httpClient.get(url);
        if (monitorHttpResponse.isPresent()) {
            int status = monitorHttpResponse.get().getStatus();
            metricsService.increment(String.valueOf(status));
            LOG.info("Response Status: {}", status);
            if (status == 200) {
                histogram.incrementSuccess();
            } else {
                histogram.incrementNon200();
            }
        } else {
            metricsService.increment("http-error");
            LOG.info("Response Status: http-error");
            histogram.incrementHttpCallError();
        }
    }
}
