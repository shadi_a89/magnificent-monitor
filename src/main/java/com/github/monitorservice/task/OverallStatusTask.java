package com.github.monitorservice.task;

import com.github.monitorservice.service.HttpStatusHistogram;
import com.github.monitorservice.service.SuccessPercentageService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class OverallStatusTask implements Runnable {

    private static final Logger LOG = LogManager.getLogger(OverallStatusTask.class);

    private final HttpStatusHistogram histogram;
    private final SuccessPercentageService percentageService;

    @Inject
    public OverallStatusTask(HttpStatusHistogram histogram,
                             SuccessPercentageService percentageService) {
        this.histogram = histogram;
        this.percentageService = percentageService;
    }


    @Override
    public void run() {
        long response200 = histogram.getResponse200().get();
        long responseHttpCallError = histogram.getResponseHttpCallError().get();
        long responseNon200 = histogram.getResponseNon200().get();

        float successPercentage = percentageService.findHttpSuccessPercentage(response200,
                                                                              responseNon200,
                                                                              responseHttpCallError);

        LOG.info("Success rate: {}%", successPercentage);
    }
}
