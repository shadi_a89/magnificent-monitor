package com.github.monitorservice;

import com.github.monitorservice.metrics.StatsdClientProvider;
import com.google.inject.AbstractModule;
import com.google.inject.name.Names;
import com.timgroup.statsd.StatsDClient;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import static com.google.inject.name.Names.named;

public class MonitorServiceModule extends AbstractModule {

    private static final String PROPERTIES_FILE = "application.properties";

    public static final String MONITOR_EXECUTOR = "monitor-executor";
    public static final String REPORT_EXECUTOR = "report-executor";

    @Override
    protected void configure() {
        Names.bindProperties(binder(), readProperties());

        bind(StatsDClient.class).toProvider(StatsdClientProvider.class).asEagerSingleton();

        bind(ScheduledExecutorService.class).annotatedWith(named(MONITOR_EXECUTOR))
                .toInstance(Executors.newSingleThreadScheduledExecutor());

        bind(ScheduledExecutorService.class).annotatedWith(named(REPORT_EXECUTOR))
                .toInstance(Executors.newSingleThreadScheduledExecutor());
    }

    private Properties readProperties() {
        Properties properties = new Properties();
        try (InputStreamReader reader = new InputStreamReader(getClass().getClassLoader()
                                                                      .getResourceAsStream(PROPERTIES_FILE))) {
            properties.load(reader);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties;
    }
}
