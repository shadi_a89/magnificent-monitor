package com.github.monitorservice.service;

import com.github.monitorservice.task.HeartbeatMonitorTask;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static com.github.monitorservice.MonitorServiceModule.MONITOR_EXECUTOR;

@Singleton
public class MonitorService {

    private final ScheduledExecutorService executorService;
    private final HeartbeatMonitorTask heartbeatMonitorTask;
    private final int monitorFrequency;

    @Inject
    public MonitorService(@Named(MONITOR_EXECUTOR)ScheduledExecutorService executorService,
                          HeartbeatMonitorTask heartbeatMonitorTask,
                          @Named("app.monitor.freq.seconds") int monitorFrequency) {
        this.executorService = executorService;
        this.heartbeatMonitorTask = heartbeatMonitorTask;
        this.monitorFrequency = monitorFrequency;
    }

    public void start() {
        executorService.scheduleAtFixedRate(heartbeatMonitorTask, 0, monitorFrequency, TimeUnit.SECONDS);
    }

    public void shutDown() {
        executorService.shutdown();
    }
}
