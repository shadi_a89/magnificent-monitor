package com.github.monitorservice.service;

import javax.inject.Singleton;
import java.util.concurrent.atomic.AtomicLong;

@Singleton
public class HttpStatusHistogram {

    private final AtomicLong response200 = new AtomicLong(0);
    private final AtomicLong responseNon200 = new AtomicLong(0);
    private final AtomicLong responseHttpCallError = new AtomicLong(0);

    public void incrementSuccess(){
        response200.addAndGet(1);
    }

    public void incrementNon200() {
        responseNon200.addAndGet(1);
    }

    public void incrementHttpCallError() {
        responseHttpCallError.addAndGet(1);
    }

    public AtomicLong getResponse200() {
        return response200;
    }

    public AtomicLong getResponseNon200() {
        return responseNon200;
    }

    public AtomicLong getResponseHttpCallError() {
        return responseHttpCallError;
    }
}
