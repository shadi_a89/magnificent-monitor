package com.github.monitorservice.service.http;

import java.util.Objects;

public class MonitorHttpResponse {

    private final int status;
    private final String response;

    public MonitorHttpResponse(int status, String response) {
        this.status = status;
        this.response = response;
    }

    public int getStatus() {
        return status;
    }

    public String getResponse() {
        return response;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MonitorHttpResponse that = (MonitorHttpResponse) o;
        return status == that.status &&
                Objects.equals(response, that.response);
    }

    @Override
    public int hashCode() {
        return Objects.hash(status, response);
    }

    @Override
    public String toString() {
        return "HttpResponse{" +
                "status=" + status +
                ", response='" + response + '\'' +
                '}';
    }
}
