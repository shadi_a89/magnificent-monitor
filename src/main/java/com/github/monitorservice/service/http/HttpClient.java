package com.github.monitorservice.service.http;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@Singleton
public class HttpClient {

    private static final Logger LOG = LogManager.getLogger(HttpClient.class);

    @Inject
    public HttpClient() {
    }

    public Optional<MonitorHttpResponse> get(final String url) {
        try {
            Future<com.mashape.unirest.http.HttpResponse<String>> httpResponseFuture = Unirest.get(url).asStringAsync();
            HttpResponse<String> stringHttpResponse = httpResponseFuture.get(5, TimeUnit.SECONDS);

            return Optional.of(new MonitorHttpResponse(stringHttpResponse.getStatus(), stringHttpResponse.getBody()));
        } catch (ExecutionException | TimeoutException e) {
            LOG.error(e);
        } catch (InterruptedException e) {
            LOG.error("Error while getting http call result", e);
            throw new RuntimeException(e);
        }
        return Optional.empty();
    }
}
