package com.github.monitorservice.service;

import com.github.monitorservice.task.OverallStatusTask;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static com.github.monitorservice.MonitorServiceModule.REPORT_EXECUTOR;

@Singleton
public class OverallStatusReporter {

    private final ScheduledExecutorService executorService;
    private final OverallStatusTask overallStatusTask;
    private final int reportFreq;

    @Inject
    public OverallStatusReporter(@Named(REPORT_EXECUTOR) ScheduledExecutorService executorService,
                                 OverallStatusTask overallStatusTask,
                                 @Named("app.report.freq.seconds") int reportFreq) {
        this.executorService = executorService;
        this.overallStatusTask = overallStatusTask;
        this.reportFreq = reportFreq;
    }

    public void startReporting() {
        executorService.scheduleAtFixedRate(overallStatusTask, reportFreq, reportFreq, TimeUnit.SECONDS);
    }
}
