package com.github.monitorservice.service;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class SuccessPercentageService {

    @Inject
    public SuccessPercentageService() {
    }

    public float findHttpSuccessPercentage(long count200, long non200Count, long errorCount) {
        return (count200 * 100.0f) / (count200 + non200Count + errorCount);
    }
}
