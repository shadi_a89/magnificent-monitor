FROM greyfoxit/alpine-openjdk8

MAINTAINER user

COPY target/magnificent-monitor-1.0-SNAPSHOT-jar-with-dependencies.jar app.jar

ENTRYPOINT ["sh", "-c", "java $JAVA_OPTS -jar /app.jar"]